require 'server_side/sse'

class MessagingController < ApplicationController
  include ActionController::Live

  def index
  end

  def add_widget
    widget = Widget.new name: 'testing'
    widget.save!

    render json: widget
  end

  # def send_message
  #   response.headers['Content-Type'] = 'text/event-stream'
  #   # sse = ServerSide::SSE.new(response.stream)
  #   # begin
  #   #   loop do
  #   #     sse.write({ message: "#{params[:message]}" })
  #   #     sleep 1
  #   #   end
  #   #
  #   # rescue IOError
  #   # ensure
  #   #   sse.close
  #   # end
  #
  #
  #   10.times {
  #     response.stream.write "#{params[:message]}a\n"
  #     sleep 1
  #   }
  #
  #   response.stream.close
  # end
end
