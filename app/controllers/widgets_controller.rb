class WidgetsController < FayeRails::Controller
  channel '/widgets' do
    monitor :subscribe do
      puts "Client #{client_id} subscribed to #{channel}."
    end
    monitor :unsubscribe do
      puts "Client #{client_id} unsubscribed from #{channel}."
    end
    monitor :publish do
      puts "Client #{client_id} published #{data.inspect} to #{channel}."
    end
  end

  observe Widget, :after_create do |new_widget|
    puts new_widget
    WidgetsController.publish('/widgets', new_widget.attributes)
  end
end