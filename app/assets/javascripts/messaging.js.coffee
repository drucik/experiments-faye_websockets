# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
client = new Faye.Client('/faye')

client.subscribe '/widgets', (payload)->
  time = moment(payload.created_at).format('D/M/YYYY H:mm:ss')
  # You probably want to think seriously about XSS here:
  $('#log').append("<li>#{time}, #{payload.id} #{payload.name}</li>")

# in case anyone wants to play with the inspector.
window.client = client