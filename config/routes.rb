Rails.application.routes.draw do
  root 'messaging#index'
  get 'messaging' => 'messaging#send_message', :as => 'messaging'
  get 'send' => 'messaging#add_widget'
end
